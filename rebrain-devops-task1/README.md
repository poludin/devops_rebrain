<p align="center"><a href="https://github.com/poludin" target="_blank"><img src="https://avatars.githubusercontent.com/u/70154853?v=4" width="400"></a></p>

## About me

My name is Alexander Poludin. At the moment I am working in the biotechnological company Geropharm, as the head of the department. The company specializes in baking medicines for diabetics - insulin.

Company link: 

- [Geropharm](https://www.geropharm.ru/).

## Education

I have two higher educations:

 1. Software for computer technology and automated systems
 2. Biomedical engineering 

For almost 7 years I have been working on a second education. Working as a boss - I feel like a manager. And this role is not very much and I like it, I like working more at the "machine".

And I also want to change something in my work, to move in a different direction.

I don’t want my first diploma to just lie there. From childhood I loved computers and digging in them. So I decided to go to the REBRAIN courses.

I would like to constantly develop and learn new things.


## [REBRAIN](https://rebrainme.com/)

During the courses, I bought educational packages for:

- **DevOps**
- **Docker**
- **Kubernetes v2**
- **Bash**

I hope I can go through everything and change the scope of work. It won't be easy though.

## LinkedIn

My LinkedIn Profile - [link](https://www.linkedin.com/in/poludin/).
